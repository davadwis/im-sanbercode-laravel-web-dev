<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

function getAnimal($animal)
{
    echo "Name: $animal->name <br>";
    echo "Legs: $animal->legs <br>";
    echo "Cold Blooded: $animal->cold_blooded <br>";
}

getAnimal($sheep);
echo "<br>";

getAnimal($sungokong);
$sungokong->yell();
echo "<br>";

getAnimal($kodok);
$kodok->jump();

// echo $sheep->name;
// echo $sheep->legs;
// echo $sheep->cold_blooded;
