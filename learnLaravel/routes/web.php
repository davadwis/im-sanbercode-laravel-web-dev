<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\GenreController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [HomeController::class, "home"]);
Route::get("/register", [AuthController::class, "register"]);
Route::post("/welcome", [AuthController::class, "welcome"]);

Route::group(['prefix' => 'dashboard'], function () {
    Route::get("/", [DashboardController::class, "dashboard"]);
    Route::group(['prefix' => 'data-master'], function () {
        Route::get("/table", [TableController::class, "table"]);
        Route::get("/data-tables", [TableController::class, "dataTables"]);
    });
});

Route::group(['prefix' => 'cast'], function () {
    Route::get('/', [CastController::class, 'index']);
    Route::get('/create', [CastController::class, 'create']);
    Route::post('/', [CastController::class, 'store']);
    Route::get('/{id}', [CastController::class, 'show']);
    Route::get('/{id}/edit', [CastController::class, 'edit']);
    Route::put('/{id}', [CastController::class, 'update']);
    Route::delete('/{id}', [CastController::class, 'destroy']);
});

Route::resource('genre', GenreController::class);

Route::resource('film', FilmController::class);