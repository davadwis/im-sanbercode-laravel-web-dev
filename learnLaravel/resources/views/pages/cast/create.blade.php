@extends("layout/base")

@section("title")
Add Cast
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <form action="/cast" method="post">
            @csrf

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Cast Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Age</label>
                <input type="number" name="age" class="form-control" placeholder="Enter Age">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Bio</label>
                <textarea type="text" name="bio" class="form-control" placeholder="Enter Bio"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection