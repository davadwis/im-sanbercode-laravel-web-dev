@extends("layout/base")

@section("title")
Edit Cast
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <form action="/cast/{{$casts -> id}}" method="post">
            @csrf
            @method("put")

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Cast Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{$casts -> name}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Age</label>
                <input type="number" name="age" class="form-control" placeholder="Enter Age" value="{{$casts -> age}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Bio</label>
                <textarea type="text" name="bio" class="form-control" placeholder="Enter Bio">{{$casts -> bio}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>

@endsection