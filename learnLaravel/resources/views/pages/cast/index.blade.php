@extends("layout/base")

@section("title")
Data Casts
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <a href="/cast/create" class="btn btn-primary btn-sm my-2">Create</a>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($casts as $index => $cast)
                <tr>
                    <th scope="row">{{ $index + 1 }}</th>
                    <td>{{ $cast -> name }}</td>
                    <td>{{ $cast -> age }}</td>
                    <td>
                        <form action="/cast/{{$cast->id}}" method="post">
                            <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method("delete")
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td>No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

@endsection