@extends("layout/base")

@section("title")
Data Detail Cast
@endsection

@section("content")
<div class="card">
    <div class="card-header">
        <h2>Detail Cast</h2>
    </div>
    <div class="card-body">
        <h2>{{$casts -> name}}</h2>
        <p>Age: <span>{{$casts -> age}}</span></p>
        <p>Bio: <span>{{$casts -> bio}}</span></p>
        <a href="/cast" class="btn btn-primary btn-sm">Back</a>
    </div>
</div>
@endsection