@extends("layout/base")

@section("title")
Add Films
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <form action="/film" method="post" enctype="multipart/form-data">
            @csrf

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" name="title" class="form-control" placeholder="Enter Title">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Synopsis</label>
                <textarea type="text" name="synopsis" class="form-control" placeholder="Enter Synopsis"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Year</label>
                <input type="number" name="year" class="form-control" placeholder="Enter Year Release">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Poster</label>
                <input type="file" name="poster" class="form-control" placeholder="Enter Poster">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Genre</label>
                <select name="genre_id" class="form-control">
                    <option value="">-- Choose Genre --</option>
                    @forelse ($genres as $genre)
                    <option value="{{$genre->id}}">{{$genre->name}}</option>
                    @empty
                    <option value="">No Data</option>
                    @endforelse
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection