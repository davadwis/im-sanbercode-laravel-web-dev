@extends("layout/base")

@section("title")
Data Detail Film
@endsection

@section("content")
<div class="card p-2">
    <div class="d-flex">
        <img class="card-img-top rounded mx-auto" src="{{asset('image/' .  $film->poster)}}" alt="{{$film->title}}" style="height: 500px; width: auto;">
        <div class="card-body">
            <h3>{{$film->title}} <span>({{$film->year}})</span></h3>
            <p class="text-sm">Genre: </p>
            <p class="card-text">{{$film->synopsis}}</p>
            <a href="/film" class="btn btn-primary btn-sm">Back</a>
        </div>
    </div>
</div>
@endsection