@extends("layout/base")

@section("title")
Films
@endsection

@section("content")
<div class="ml-3">
    <a href="/film/create" class="btn btn-primary btn-sm px-3 mb-2">Add New Film</a>
</div>

<div class="row mx-2">
    @forelse ($film as $film)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top rounded" src="{{asset('image/' .  $film->poster)}}" alt="{{$film->title}}" style="height: 350px;">
            <div class="card-body">
                <h5>{{$film->title}} <span>({{$film->year}})</span></h5>
                <p class="card-text">{{Str::limit($film->synopsis, 20)}}</p>
                <a href="/film/{{$film->id}}" class="btn btn-info btn-block btn-sm">Detail Film</a>
                <div class="row my-2 gap-2">
                    <div class="col">
                        <a href="/film/{{$film->id}}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/film/{{$film->id}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
    <h2>No Data</h2>
    @endforelse
</div>



@endsection