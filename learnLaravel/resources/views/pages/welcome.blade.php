<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>

<body>
    <h1>SELAMAT DATANG {{$firstname}} {{$lastname}}!</h1>
    <h2>
        Terimakasih telah bergabung di SanberBook. Social Media kita bersama!
    </h2>
    <h3>
        Berikut data diri kamu:
    </h3>
    <ul>
        <li>Nama Depan: {{$firstname}}</li>
        <li>Nama Belakang: {{$lastname}}</li>
        <li>Negara: {{$nationality}}</li>
        <li>Jenis Kelamin:
            @if ($gender === "male")
            Laki-Laki
            @else
            Perempuan
            @endif
        </li>
    </ul>
</body>

</html>