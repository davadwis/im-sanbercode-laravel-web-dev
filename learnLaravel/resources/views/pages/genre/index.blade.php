@extends("layout/base")

@section("title")
Data Casts
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <a href="/genre/create" class="btn btn-primary btn-sm my-2">Create</a>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($genres as $index => $genre)
                <tr>
                    <th scope="row">{{ $index + 1 }}</th>
                    <td>{{ $genre -> name }}</td>
                    <td>
                        <form action="/genre/{{$genre->id}}" method="post">
                            <a href="/genre/{{$genre->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/genre/{{$genre->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method("delete")
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td>No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

@endsection