@extends("layout/base")

@section("title")
Add Genre
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <form action="/genre" method="post">
            @csrf

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Gnere Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Genre">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection
