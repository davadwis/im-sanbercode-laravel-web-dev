@extends("layout/base")

@section("title")
Edit Cast
@endsection

@section("content")

<div class="card">
    <div class="card-body">
        <form action="/genre/{{$genre -> id}}" method="post">
            @csrf
            @method("put")

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Genre Name</label>
                <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{$genre -> name}}">
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
</div>

@endsection