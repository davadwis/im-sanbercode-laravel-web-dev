@extends("layout/base")

@section("title")
Data Detail Genre
@endsection

@section("content")
<div class="card">
    <div class="card-header">
        <h2>Detail Genre</h2>
    </div>
    <div class="card-body">
        <h2>{{$genre -> name}}</h2>
        <a href="/genre" class="btn btn-primary btn-sm">Back</a>
    </div>
</div>
@endsection