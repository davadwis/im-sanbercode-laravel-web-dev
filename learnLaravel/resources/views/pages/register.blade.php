<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Akun</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">Firstname:</label><br />
        <input type="text" name="firstname" id="firstname" /><br /><br />

        <label for="firstname">Lastname:</label><br />
        <input type="text" name="lastname" id="lastname" /><br /><br />

        <label for="gender">Gender:</label><br />
        <input type="radio" name="gender" value="male" />Male<br />
        <input type="radio" name="gender" value="female" />Female<br /><br />

        <label for="nationality">Nationality:</label><br />
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="philipines">Philipines</option>
            <option value="malaysian">Malaysian</option>
        </select><br /><br />

        <label for="language">Language Spoken:</label><br />
        <input type="checkbox" />Bahasa Indonesia<br />
        <input type="checkbox" />Tagalog<br />
        <input type="checkbox" />Melayu<br />
        <input type="checkbox" />English<br />
        <input type="checkbox" />Other<br /><br />

        <label for="bio">Bio:</label><br />
        <textarea name="bio" id="bio" cols="20" rows="10"></textarea><br /><br />

        <button type="submit" value="welcome">Sign Up</button>
    </form>
</body>

</html>