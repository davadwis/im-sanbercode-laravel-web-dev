<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FilmController extends Controller
{
    public function index()
    {
        $film = Film::all();
        return view('pages/film/index', compact('film'));
    }

    public function create()
    {
        $genres = Genre::get();
        return view('pages/film/create', ['genres' => $genres]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'synopsis' => 'required',
            'year' => 'required',
            'poster' => 'required|image|mimes:png,jpg,jpeg',
            'genre_id' => 'required',
        ]);

        $fileName = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('image'), $fileName);

        Film::create([
            'title' => $request->title,
            'synopsis' => $request->synopsis,
            'year' => $request->year,
            'poster' => $fileName,
            'genre_id' => $request->genre_id,
        ]);

        return redirect("/film");
    }

    public function show($id)
    {
        $film = Film::find($id);
        // dd($film);
        return view('pages/film/detail', compact('film'));
    }

    public function edit($id)
    {
        $film = Film::find($id);
        $genres = Genre::get();
        return view('pages/film/edit', compact(['film', 'genres']));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'synopsis' => 'required',
            'year' => 'required',
            'poster' => 'image|mimes:png,jpg,jpeg',
            'genre_id' => 'required',
        ]);

        $film = Film::find($id);

        if ($request->has('poster')) {
            $path = 'image/';

            if ($film->poster) {
                File::delete($path . $film->poster);
            }

            $fileName = time() . '.' . $request->poster->extension();
            $request->poster->move(public_path('image'), $fileName);

            $film->poster = $fileName;
        }

        $film->title = $request->title;
        $film->synopsis = $request->synopsis;
        $film->year = $request->year;
        $film->genre_id = $request->genre_id;

        $film->update();

        return redirect('/film')->with('success', 'Film updated successfully');
    }

    public function destroy($id)
    {
        $film = Film::find($id);
        $film->delete();
        return redirect('/film');
    }
}
