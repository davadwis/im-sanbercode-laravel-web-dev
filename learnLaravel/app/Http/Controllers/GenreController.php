<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index()
    {
        $genres = Genre::all();
        return view('pages/genre/index', compact('genres'));
    }

    public function create()
    {
        return view('pages/genre/create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        Genre::create([
            'name' => $request->name,
        ]);

        return redirect('/genre');
    }

    public function show($id)
    {
        $genre = Genre::find($id);
        return view('pages/genre/detail', compact('genre'));
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('pages/genre/edit', compact('genre'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:genres',
        ]);

        $genre = Genre::find($id);
        $genre->name = $request->name;
        $genre->update();
        return redirect('/genre');
    }

    public function destroy($id)
    {
        $genre = Genre::find($id);
        $genre->delete();
        return redirect('/genre');
    }
}
