<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(): View
    {
        return view('pages/register');
    }

    public function welcome(Request $request): View
    {
        return view('pages/welcome', [
            "firstname" => $request["firstname"],
            "lastname" => $request["lastname"],
            "gender" => $request["gender"],
            "nationality" => $request["nationality"],
        ]);
    }
}
