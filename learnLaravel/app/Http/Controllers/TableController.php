<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class TableController extends Controller
{
    public function table(): View
    {
        return view("pages/table");
    }

    public function dataTables(): View
    {
        return view("pages/data-tables");
    }
}
