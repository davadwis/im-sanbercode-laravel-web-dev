<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $data = DB::table('casts')->get();
        // dd($data);
        return view('pages/cast/index', ['casts' => $data]);
    }

    public function create(): View
    {
        return view('pages/cast/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:4',
            'age' => 'required',
            'bio' => 'required|min:6',
        ]);

        DB::table('casts')->insert([
            'name' => $request->input('name'),
            'age' => $request->input('age'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function show($params)
    {
        $data = DB::table('casts')->find($params);

        return view("pages/cast/detail", ['casts' => $data]);
    }

    public function edit($params)
    {
        $data = DB::table('casts')->find($params);

        return view('/pages/cast/edit', ['casts' => $data]);
    }

    public function update($params, Request $request)
    {
        $request->validate([
            'name' => 'required|min:4',
            'age' => 'required',
            'bio' => 'required|min:6',
        ]);

        DB::table('casts')
            ->where('id', $params)
            ->update([
                'name' => $request->input('name'),
                'age' => $request->input('age'),
                'bio' => $request->input('bio'),
            ]);

        return redirect("/cast");
    }

    public function destroy($params)
    {
        DB::table('casts')->where('id', $params)->delete();

        return redirect('/cast');
    }
}
